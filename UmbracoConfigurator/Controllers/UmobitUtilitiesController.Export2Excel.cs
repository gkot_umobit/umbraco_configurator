﻿using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.PropertyEditors;
using UmbracoConfigurator.Helpers;
using UmbracoConfigurator.Models;

namespace Umobit.UmbracoConfigurator.Controllers
{
    public partial class UmobitUtilitiesController : UmbracoAuthorizedController //SurfaceController
    {
        private string BackupPath { get; set; } = "\\UmbracoConfigurator";

        private List<Exception> Exceptions = new List<Exception>();

        public ActionResult Utilities()
        {
            return View();
        }

        public ActionResult ExportNodeDescendantsToExcelView(int nodeId)
        {
            return View(new { NodeId = nodeId });
        }

        public JsonResult ExportNodeDescendantsToExcel(int nodeId, bool includeSelf, bool includeNodeNames, bool includePropertyNames)
        {

            string filename = "Export_Nodes_" + DateTime.Now.ToString("yyyyMMdd_HHmm") + ".xlsx";

            //var container = Services.EntityService.GetDescendants(-1).FirstOrDefault(x => x.Name == "Widgets");
            //Services.ContentTypeService.DeleteContainer(container.Id);
            if (!Directory.Exists(Server.MapPath("~" + BackupPath)))
            {
                Directory.CreateDirectory(Server.MapPath("~" + BackupPath));
            }

            FileInfo fileInfo = new FileInfo(Server.MapPath("~" + BackupPath + "\\" + filename));

            var contentService = Services.ContentService;

            using (var package = new ExcelPackage(fileInfo))
            {

                try
                {
                    IPublishedContent parentNode = UmbracoContext.Content.GetById(nodeId); //contentService.GetById(nodeId);
                    var cultures = parentNode.Cultures.Keys;
                    foreach (var culture in cultures)
                    {

                        //var cultures = parentNode.AvailableCultures;
                        //var parentNodeName = parentNode.CultureInfos[cultures.First()].Name;
                        var ws = package.Workbook.Worksheets.Add(string.Format("{0} [{1}]", parentNode.Name, culture.ToUpper()));
                        int row = 1;
                        int column = 1;


                        var descendants = (includeSelf ? parentNode.DescendantsOrSelf(culture) : parentNode.Descendants(culture)).ToList();

                        ProcessNodes(descendants, includeNodeNames, includePropertyNames, ref ws, ref row, ref column);


                        ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    }
                    if (package.Workbook.Worksheets.Count == 0)
                    {
                        var ws = package.Workbook.Worksheets.Add(string.Format("{0}", parentNode.Name));
                        int row = 1;
                        int column = 1;


                        var descendants = (includeSelf ? parentNode.DescendantsOrSelf() : parentNode.Descendants()).ToList();

                        ProcessNodes(descendants, includeNodeNames, includePropertyNames, ref ws, ref row, ref column);
                    }
                    package.Save();

                }
                catch (Exception ex)
                {

                    Exceptions.Add(ex);
                }
            }

            if (Exceptions.Count > 0)
            {
                var exceptions = Exceptions.Select(ex => new
                {
                    exceptionType = ex.GetType()?.Name,
                    message = ex.Message,
                    innerExceptionMessage = ex.InnerException?.Message,
                    data = ex.Data,
                    source = ex.Source,
                    targetSiteName = ex.TargetSite.Name,
                    stackTrace = ex.StackTrace,
                }).ToList();
                return Json(new
                {
                    success = false,
                    message = "EXCEPTIONS Occurred",
                    exceptions = exceptions
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, message = "Excel file created successfully", link = (BackupPath + "\\" + filename).Replace("\\", "/") }, JsonRequestBehavior.AllowGet);
        }

        private void ProcessNodes(IEnumerable<IPublishedElement> descendants, bool includeNodeNames, bool includePropertyNames, ref ExcelWorksheet ws, ref int row, ref int column)
        {
            string contentTypeAlias = "";
            int startingColumn = column;
            foreach (var node in descendants)
            {
                if (includeNodeNames)
                {
                    ws.Cells[row, column++].Value = string.Format("{0}", (node is IPublishedContent) ? (node as IPublishedContent).Name : node.ContentType.Alias);
                }

                if (includePropertyNames && node.ContentType.Alias != contentTypeAlias)
                {
                    contentTypeAlias = node.ContentType.Alias;
                    foreach (var property in node.Properties)
                    {
                        if (IsPropertyValidForExcel(property))
                        {
                            ws.Cells[row, column++].Value = property.Alias;
                        }
                    }

                    row++;
                    column = startingColumn;
                }
                foreach (var property in node.Properties)
                {
                    ProcessPropertyForExcel(property, includeNodeNames, includePropertyNames, ref ws, ref row, ref column);
                }

                row++;
                column = startingColumn;

            }
        }



        private void ProcessPropertyForExcel(IPublishedProperty property, bool includeNodeNames, bool includePropertyNames, ref ExcelWorksheet ws, ref int row, ref int column)
        {
            if (IsPropertyValidForExcel(property))
            {
                switch (property.PropertyType.EditorAlias)
                {
                    case "Umbraco.TextBox":
                        ws.Cells[row, column++].Value = property.Value();
                        break;
                    case "Umbraco.NestedContent":
                        if (property.PropertyType.ClrType.Name.Contains("IEnumerable"))
                        {
                            row++;
                            var k = property.Value() as IEnumerable<object>;
                            var t = property.Value() as IEnumerable<IPublishedElement>;
                            var nodes = property.Value<IEnumerable<IPublishedElement>>();
                            ProcessNodes(nodes, includeNodeNames, includePropertyNames, ref ws, ref row, ref column);
                            row--;


                        }
                        //if(property.PropertyType.ModelClrType.)
                        break;
                }

            }
        }


        public JsonResult ExportNodeDescendantsToExcel2(int nodeId, bool includeSelf, bool includeNodeNames, bool includePropertyNames)
        {

            string filename = "Export_Nodes_" + DateTime.Now.ToString("yyyyMMdd_HHmm") + ".xlsx";

            //var container = Services.EntityService.GetDescendants(-1).FirstOrDefault(x => x.Name == "Widgets");
            //Services.ContentTypeService.DeleteContainer(container.Id);
            if (!Directory.Exists(Server.MapPath("~" + BackupPath)))
            {
                Directory.CreateDirectory(Server.MapPath("~" + BackupPath));
            }

            FileInfo fileInfo = new FileInfo(Server.MapPath("~" + BackupPath + "\\" + filename));

            var contentService = Services.ContentService;

            long totalRecords = 0;

            using (var package = new ExcelPackage(fileInfo))
            {

                try
                {
                    IContent parentNode = contentService.GetById(nodeId); //contentService.GetById(nodeId);
                    var cultures = parentNode.CultureInfos.Keys;
                    foreach (var culture in cultures)
                    {





                        var descendants = contentService.GetPagedDescendants(parentNode.Id, 0, 10000, out totalRecords).ToList();
                        if (descendants.All(x => x.CultureInfos.Keys.Contains(culture)))
                        {
                            //var cultures = parentNode.AvailableCultures;
                            //var parentNodeName = parentNode.CultureInfos[cultures.First()].Name;
                            var ws = package.Workbook.Worksheets.Add(string.Format("{0} [{1}]", parentNode.Name, culture.ToUpper()));
                            int row = 1;
                            int column = 1;
                            if (includeSelf)
                            {
                                descendants.Insert(0, parentNode);
                            }

                            ProcessNodes2(descendants, includeNodeNames, includePropertyNames, culture, ref ws, ref row, ref column);

                            if (ws.Dimension != null)
                                ws.Cells[ws.Dimension.Address].AutoFitColumns();
                        }
                    }
                    if (package.Workbook.Worksheets.Count == 0)//if no cultures for node
                    {
                        var ws = package.Workbook.Worksheets.Add(string.Format("{0}", parentNode.Name));
                        int row = 1;
                        int column = 1;


                        var descendants = contentService.GetPagedDescendants(parentNode.Id, 0, 10000, out totalRecords).ToList();
                        if (includeSelf)
                        {
                            descendants.Insert(0, parentNode);
                        }

                        ProcessNodes2(descendants, includeNodeNames, includePropertyNames, "", ref ws, ref row, ref column);

                        if (ws.Dimension != null)
                            ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    }

                    package.Save();


                }
                catch (Exception ex)
                {

                    Exceptions.Add(ex);
                }
            }

            if (Exceptions.Count > 0)
            {
                var exceptions = Exceptions.Select(ex => new
                {
                    exceptionType = ex.GetType()?.Name,
                    message = ex.Message,
                    innerExceptionMessage = ex.InnerException?.Message,
                    data = ex.Data,
                    source = ex.Source,
                    targetSiteName = ex.TargetSite.Name,
                    stackTrace = ex.StackTrace,
                }).ToList();
                return Json(new
                {
                    success = false,
                    message = "EXCEPTIONS Occurred",
                    exceptions = exceptions
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, message = "Excel file created successfully", link = (BackupPath + "\\" + filename).Replace("\\", "/") }, JsonRequestBehavior.AllowGet);
        }
        private void ProcessNodes2(IEnumerable<IContent> descendants, bool includeNodeNames, bool includePropertyNames, string culture, ref ExcelWorksheet ws, ref int row, ref int column)
        {
            string contentTypeAlias = "";
            int startingColumn = column;
            foreach (var node in descendants)
            {
                if (includeNodeNames)
                {
                    ws.Cells[row, column++].Value = string.Format("{0}", (node is IPublishedContent) ? (node as IPublishedContent).Name : node.ContentType.Alias);
                }

                if (includePropertyNames && node.ContentType.Alias != contentTypeAlias)
                {
                    contentTypeAlias = node.ContentType.Alias;
                    foreach (var property in node.Properties)
                    {
                        if (IsPropertyValidForExcel(property))
                        {
                            ws.Cells[row, column++].Value = Services.ContentTypeService.Get(node.ContentType.Id)
                                                                        .PropertyTypes.FirstOrDefault(x => x.Alias == property.Alias).Name;
                        }
                    }

                    row++;
                    column = startingColumn;
                }
                foreach (var propertyType in Services.ContentTypeService.Get(node.ContentType.Id).PropertyGroups.FirstOrDefault().PropertyTypes.Select(x => x))//TODO: ORDERING
                {
                    var property = node.Properties.FirstOrDefault(x => x.PropertyType.Id == propertyType.Id);
                    ProcessPropertyForExcel2(property, includeNodeNames, includePropertyNames, culture, ref ws, ref row, ref column);
                }

                row++;
                column = startingColumn;

            }
        }

        private void ProcessPropertyForExcel2(Property property, bool includeNodeNames, bool includePropertyNames, string culture, ref ExcelWorksheet ws, ref int row, ref int column)
        {
            if (IsPropertyValidForExcel(property))
            {
                if (property.Values.Count(x => x.Culture == culture || (string.IsNullOrEmpty(culture) && string.IsNullOrEmpty(x.Culture))) > 0)
                {
                    switch (property.PropertyType.PropertyEditorAlias)
                    {
                        case "Umbraco.TextBox":
                        case "Umbraco.Label":
                            ws.Cells[row, column++].Value = property.Values.FirstOrDefault(x => x.Culture == culture || (string.IsNullOrEmpty(culture) && string.IsNullOrEmpty(x.Culture))).EditedValue;
                            break;
                        case "Umbraco.NestedContent":
                            int startingColumn = column;


                            var value = property.Values.FirstOrDefault(x => x.Culture == culture || (string.IsNullOrEmpty(culture) && string.IsNullOrEmpty(x.Culture))).EditedValue.ToString();
                            var serializer = new JavaScriptSerializer(); //using System.Web.Script.Serialization;

                            var nodes = serializer.Deserialize<IEnumerable<Dictionary<string, string>>>(value);




                            string contentTypeAlias = nodes.FirstOrDefault()["ncContentTypeAlias"];

                            foreach (var node in nodes)
                            {
                                if (includeNodeNames)
                                {
                                    ws.Cells[row, column++].Value = string.Format("{0}", node["ncContentTypeAlias"]);
                                }
                                var properties = node.Where(x => !new string[] { "key", "name", "ncContentTypeAlias" }.Contains(x.Key));
                                if (includePropertyNames && node["ncContentTypeAlias"] != contentTypeAlias)
                                {
                                    contentTypeAlias = node["ncContentTypeAlias"];
                                    foreach (var p in properties)
                                    {

                                        ws.Cells[row, column++].Value = p.Key;

                                    }

                                    row++;
                                    column = startingColumn;
                                }



                                foreach (var p in properties)
                                {
                                    row++;
                                    ws.Cells[row, column++].Value = p.Value;

                                }
                                column = startingColumn;
                                // [{"key":"27a4650d-4215-49a9-9fcf-84153ea51dd9","name":"Item 1","ncContentTypeAlias":"testOne","property1":"hey element 1 value"},{"key":"b5683eee-92d8-4f30-a27d-69cef789567c","name":"Item 2","ncContentTypeAlias":"testOne","property1":"Hey item 2 value"}]
                            }
                            column++;
                            //row--;



                            //if(property.PropertyType.ModelClrType.)
                            break;
                    }
                }
                else
                {
                    ws.Cells[row, column++].Value = " ";
                }

            }
        }

        public bool IsPropertyValidForExcel(IPublishedProperty property)
        {
            return property.PropertyType.IsUserProperty;
        }

        public bool IsPropertyValidForExcel(Property property)
        {
            return true;
        }








    }




}
