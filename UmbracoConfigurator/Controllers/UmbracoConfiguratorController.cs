﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;
using Umbraco.Web.PropertyEditors;
using UmbracoConfigurator.Helpers;
using UmbracoConfigurator.Models;

namespace Umobit.UmbracoConfigurator.Controllers
{
    public class UmbracoConfiguratorController : UmbracoAuthorizedController //SurfaceController
    {
        //private const int ELEMENTS_DELETE_COLUMN = 7;

        private string Filepath { get; set; } = "\\UmbracoConfigurator\\UmbracoConfigurator.xlsx";

        private string BackupPath { get; set; } = "\\UmbracoConfigurator";

        private List<Exception> Exceptions = new List<Exception>();

        public JsonResult ExportBackupConfiguration()
        {
            string filename = "UmbracoConfigurator_Backup_" + DateTime.Now.ToString("yyyyMMdd_HHmm") + ".xlsx";

            try
            {
                //var container = Services.EntityService.GetDescendants(-1).FirstOrDefault(x => x.Name == "Widgets");
                //Services.ContentTypeService.DeleteContainer(container.Id);

                FileInfo fileInfo = new FileInfo(Server.MapPath("~" + BackupPath + "\\" + filename));


                using (var package = new ExcelPackage(fileInfo))
                {

                    var contenTypeFolders = Services.EntityService.GetDescendants(-1, UmbracoObjectTypes.DocumentTypeContainer).ToList().Union(Services.EntityService.GetChildren(-1, UmbracoObjectTypes.DocumentType).ToList());
                    foreach (var contenTypeFolder in contenTypeFolders)
                    {
                        var ws = package.Workbook.Worksheets.Add(contenTypeFolder.Name);

                        List<ExcelContentTypeRowModel> workSheetRows = new List<ExcelContentTypeRowModel>();
                        var contentTypes = Services.ContentTypeService.GetDescendants(contenTypeFolder.Id, true).Where(x => x != null);

                        foreach (var contentType in contentTypes)
                        {
                            var parentAlias = Services.ContentTypeService.Get(contentType.ParentId)?.Alias;
                            string titleTemplate = GetTitleTemplate(contenTypeFolder.Name, contentType);
                            workSheetRows.Add(new ExcelContentTypeRowModel(contentType, parentAlias, titleTemplate));

                            foreach (var propertyGroup in contentType.PropertyGroups)
                            {
                                foreach (var propertyType in propertyGroup.PropertyTypes)
                                {
                                    var propertyDataTypeName = Services.DataTypeService.GetDataType(propertyType.DataTypeId)?.Name;
                                    workSheetRows.Add(new ExcelContentTypeRowModel(propertyType, propertyGroup.Name, propertyDataTypeName));
                                }
                            }
                        }

                        ws.Cells.LoadFromCollection(workSheetRows, true);

                        #region Clear FALSE values
                        ws.Cells[ws.Dimension.Address].ToList().ForEach(x =>
                        {
                            if (x.GetValue<string>()?.ToUpper() == "FALSE") x.Value = "";
                        });
                        #endregion Clear FALSE values
                        ws.Cells[ws.Dimension.Address].AutoFitColumns();
                    }

                    package.Save();
                }
            }
            catch (Exception ex)
            {

                Exceptions.Add(ex);
            }


            if (Exceptions.Count > 0)
            {
                var exceptions = Exceptions.Select(ex => new
                {
                    exceptionType = ex.GetType()?.Name,
                    message = ex.Message,
                    innerExceptionMessage = ex.InnerException?.Message,
                    data = ex.Data,
                    source = ex.Source,
                    targetSiteName = ex.TargetSite.Name,
                    stackTrace = ex.StackTrace,
                }).ToList();
                return Json(new
                {
                    success = false,
                    message = "EXCEPTIONS Occurred",
                    exceptions = exceptions
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, message = "Backup Configuration Ended Successfully", link = (BackupPath + "\\" + filename).Replace("\\", "/") }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Import()
        {

            try
            {
                //var container = Services.EntityService.GetDescendants(-1).FirstOrDefault(x => x.Name == "Widgets");
                //Services.ContentTypeService.DeleteContainer(container.Id);

                FileInfo fileInfo = new FileInfo(Server.MapPath("~" + Filepath));
                using (var package = new ExcelPackage(fileInfo))
                {
                    ProcessElements(package);
                    ProcessWidgets(package);
                    ProcessPages(package);
                }
            }
            catch (Exception ex)
            {

                Exceptions.Add(ex);
            }

            if (Exceptions.Count > 0)
            {
                var exceptions = Exceptions.Select(ex => new
                {
                    exceptionType = ex.GetType()?.Name,
                    message = ex.Message,
                    innerExceptionMessage = ex.InnerException?.Message,
                    data = ex.Data,
                    source = ex.Source,
                    targetSiteName = ex.TargetSite.Name,
                    stackTrace = ex.StackTrace,
                }).ToList();
                return Json(new
                {
                    success = false,
                    message = "EXCEPTIONS Occurred",
                    exceptions = exceptions
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, message = "Import Ended Successfully" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ImportSitemap()
        {

            try
            {
                //var container = Services.EntityService.GetDescendants(-1).FirstOrDefault(x => x.Name == "Widgets");
                //Services.ContentTypeService.DeleteContainer(container.Id);

                FileInfo fileInfo = new FileInfo(Server.MapPath("~" + Filepath));
                using (var package = new ExcelPackage(fileInfo))
                {
                    ProcessSitemap(package);

                }
            }
            catch (Exception ex)
            {

                Exceptions.Add(ex);
            }

            if (Exceptions.Count > 0)
            {
                var exceptions = Exceptions.Select(ex => new
                {
                    exceptionType = ex.GetType()?.Name,
                    message = ex.Message,
                    innerExceptionMessage = ex.InnerException?.Message,
                    data = ex.Data,
                    source = ex.Source,
                    targetSiteName = ex.TargetSite.Name,
                    stackTrace = ex.StackTrace,
                }).ToList();
                return Json(new
                {
                    success = false,
                    message = "EXCEPTIONS Occurred",
                    exceptions = exceptions
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = true, message = "Import Ended Successfully" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Test()
        {
            return View();
        }

        public ActionResult ConfiguratorDash()
        {
            FileInfo fileInfo = new FileInfo(Server.MapPath("~" + Filepath));

            if (fileInfo != null)
            {
                var model = new
                {
                    ImportFileIsPresent = true,
                    ImportFileName = fileInfo.Name,
                    ImportFileLastModified = fileInfo.LastWriteTime.ToString("yyyy-MM-dd HH:mm")
                };
                return View(model);
            }
            else
            {
                var model = new
                {
                    ImportFileIsPresent = false,
                };
                return View(model);
            }
            return View();
        }

        private void ProcessElements(ExcelPackage package)
        {
            var elementsSheets = package.Workbook.Worksheets.Where(x => x.Name.ToUpper().Contains("ELEMENTS"));
            foreach (var ws in elementsSheets)
            {
                //ExcelWorksheet ws = package.Workbook.Worksheets["Elements"];
                string elementsGroupName = ws.Name;

                IContentTypeService contentTypeService = Services.ContentTypeService;

                IContentType contentType = null;//new ContentType(0);
                int rowNums = ws.Dimension.Rows; // 20
                int columnNums = ws.Dimension.Columns;

                for (int rowNum = 2; rowNum <= rowNums; rowNum++)
                {

                    ExcelContentTypeRowModel row = new ExcelContentTypeRowModel(ws, rowNum);

                    if (TryGetContentTypeInfosFromRow(row, ref contentType, elementsGroupName, true))
                    {
                        if (contentType != null)
                        {
                            if (contentType.GetDirtyProperties().Count() > 0) contentType.ResetDirtyProperties();
                            contentTypeService.Save(contentType);
                        }
                        if (row.ContentTypeAlias.IsNotNullOrEmpty())//has content type
                        {

                            var container = Services.EntityService.GetDescendants(-1, UmbracoObjectTypes.DataTypeContainer).FirstOrDefault(x => x.Name == "Custom Data Types");
                            var dataType = Services.DataTypeService.GetDataType(row.ContentTypeName + "s");
                            if (!row.DeleteContentType)
                            {
                                #region Create Data Type(and Folder) If Not Created

                                if (container == null)
                                {
                                    Services.DataTypeService.CreateContainer(-1, "Custom Data Types");
                                }


                                if (dataType == null)
                                {
                                    DataEditor dataEditor = new DataEditor(Logger, EditorType.PropertyValue)
                                    {
                                        ExplicitConfigurationEditor = new NestedContentConfigurationEditor()
                                        {
                                        }
                                    };

                                    dataType = new DataType(dataEditor, container.Id)
                                    {
                                        Name = contentType.Name + "s",
                                        Editor = new NestedContentPropertyEditor(Logger, null)
                                    };


                                    (dataType.Configuration as NestedContentConfiguration).ContentTypes = new NestedContentConfiguration.ContentType[]
                                    {
                                        new NestedContentConfiguration.ContentType() { Alias = contentType.Alias, TabAlias = "Content", Template = row.ContentTypeTitleTemplate  }
                                    };

                                    //TODO: Using below line add min max etc
                                    //(dataType.Configuration as NestedContentConfiguration).MinItems = 1;

                                    Services.DataTypeService.Save(dataType);
                                }


                                #endregion Create Data Type(and Folder) If Not Created 
                            }
                            else //delete data type
                            {
                                if (dataType != null)
                                    Services.DataTypeService.Delete(dataType);
                            }
                        }
                    }
                }
            }
        }

        private void ProcessWidgets(ExcelPackage package)
        {

            var widgetSheets = package.Workbook.Worksheets.Where(x => x.Name.ToUpper().Contains("WIDGETS"));
            foreach (var ws in widgetSheets)
            {
                string widgetsGroupName = ws.Name;
                //ExcelWorksheet ws = package.Workbook.Worksheets["Elements"];
                IContentTypeService contentTypeService = Services.ContentTypeService;

                IContentType contentType = null;//new ContentType(0);
                int rowNums = ws.Dimension.Rows; // 20
                int columnNums = ws.Dimension.Columns;

                var container = Services.EntityService.GetDescendants(-1, UmbracoObjectTypes.DocumentTypeContainer).FirstOrDefault(x => x.Name == "Custom Data Types");
                var dataType = Services.DataTypeService.GetDataType(widgetsGroupName);

                #region Create Data Type(and Folder) If Not Created

                if (container == null)
                {
                    Services.DataTypeService.CreateContainer(-1, "Custom Data Types");
                }



                if (dataType == null)
                {
                    DataEditor dataEditor = new DataEditor(Logger, EditorType.PropertyValue)
                    {
                        ExplicitConfigurationEditor = new NestedContentConfigurationEditor()
                        {
                        }
                    };

                    dataType = new DataType(dataEditor, container.Id)
                    {
                        Name = widgetsGroupName,
                        Editor = new NestedContentPropertyEditor(Logger, null)
                    };


                    //(dataType.Configuration as NestedContentConfiguration).ContentTypes = new NestedContentConfiguration.ContentType[]
                    //{
                    //new NestedContentConfiguration.ContentType() { Alias = contentType.Alias, TabAlias = "Content", Template = ""  }
                    //};



                    Services.DataTypeService.Save(dataType);
                }
                #endregion Create Data Type(and Folder) If Not Created

                var dataTypeConfiguration = dataType.Configuration as NestedContentConfiguration;
                //(dataType.Configuration as NestedContentConfiguration).ContentTypes = new NestedContentConfiguration.ContentType[]
                //{
                //new NestedContentConfiguration.ContentType() { Alias = contentType.Alias, TabAlias = "Content", Template = ""  }
                //};


                for (int rowNum = 2; rowNum <= rowNums; rowNum++)
                {

                    ExcelContentTypeRowModel row = new ExcelContentTypeRowModel(ws, rowNum);

                    if (TryGetContentTypeInfosFromRow(row, ref contentType, widgetsGroupName, true))
                    {

                        if (contentType != null)
                        {
                            if (contentType.GetDirtyProperties().Count() > 0) contentType.ResetDirtyProperties();
                            contentTypeService.Save(contentType);
                        }

                        if (row.ContentTypeAlias.IsNotNullOrEmpty())//has content type
                        {
                            var widgetLineInDataType = dataTypeConfiguration.ContentTypes?.FirstOrDefault(x => x.Alias == row.ContentTypeAlias);
                            if (!row.DeleteContentType)
                            {
                                if (widgetLineInDataType == null)
                                {
                                    widgetLineInDataType = new NestedContentConfiguration.ContentType() { Alias = row.ContentTypeAlias, TabAlias = "Content", Template = row.ContentTypeTitleTemplate };
                                    if (dataTypeConfiguration.ContentTypes == null)
                                    {
                                        dataTypeConfiguration.ContentTypes = new NestedContentConfiguration.ContentType[]
                                        {
                                            widgetLineInDataType
                                        };
                                    }
                                    else
                                    {
                                        dataTypeConfiguration.ContentTypes = dataTypeConfiguration.ContentTypes.Append(widgetLineInDataType).ToArray();
                                    }
                                }
                                if (row.ContentTypeTitleTemplate.IsNotNullOrEmpty())
                                {
                                    widgetLineInDataType.Template = row.ContentTypeTitleTemplate;
                                }
                            }
                            else
                            {
                                dataTypeConfiguration.ContentTypes = dataTypeConfiguration.ContentTypes != null ? dataTypeConfiguration.ContentTypes.Where(x => x.Alias != widgetLineInDataType.Alias)?.ToArray() : null;
                            }
                            Services.DataTypeService.Save(dataType);
                        }

                    }
                }
            }


        }

        private void ProcessPages(ExcelPackage package)
        {
            var elementsSheets = package.Workbook.Worksheets.Where(x => x.Name.ToUpper().Contains("PAGES"));
            foreach (var ws in elementsSheets)
            {
                //ExcelWorksheet ws = package.Workbook.Worksheets["Elements"];
                string elementsGroupName = ws.Name;

                IContentTypeService contentTypeService = Services.ContentTypeService;

                IContentType contentType = null;//new ContentType(0);
                int rowNums = ws.Dimension.Rows; // 20
                int columnNums = ws.Dimension.Columns;

                for (int rowNum = 2; rowNum <= rowNums; rowNum++)
                {

                    ExcelContentTypeRowModel row = new ExcelContentTypeRowModel(ws, rowNum);

                    if (TryGetContentTypeInfosFromRow(row, ref contentType, elementsGroupName, false))
                    {
                        if (contentType != null)
                        {
                            if (row.ContentTypeAlias.IsNotNullOrEmpty())//has content type
                            {
                                #region Create Template if not exists

                                var templateName = row.ContentTypeName.Replace(" ", "");

                                var template = Services.FileService.GetTemplate(row.ContentTypeAlias);
                                if (template == null)
                                {
                                    template = new Template(templateName, templateName);
                                    Services.FileService.SaveTemplate(template);
                                    contentType.SetDefaultTemplate(template);
                                }

                                #endregion Create Template if not exists
                            }

                            if (contentType.GetDirtyProperties().Count() > 0) contentType.ResetDirtyProperties();
                            contentTypeService.Save(contentType);
                        }
                    }
                }
                #region Create Permissions
                for (int rowNum = 2; rowNum <= rowNums; rowNum++)
                {
                    ExcelContentTypeRowModel row = new ExcelContentTypeRowModel(ws, rowNum);

                    if (row.ContentTypeAlias.IsNotNullOrEmpty() && !row.DeleteContentType)//has content type
                    {
                        contentType = contentTypeService.Get(row.ContentTypeAlias);
                        if (contentType != null)
                        {
                            var perms = row.ContentTypePermissions.Split(',').Select(x => x.Trim()).ToList();
                            perms.Remove("");
                            contentType.AllowedAsRoot = perms.Remove("root");
                            int sort = 1;
                            if (perms.Count() > 0)
                            {
                                var ids = contentTypeService.GetAllContentTypeIds(perms.ToArray());
                                contentType.AllowedContentTypes = ids.Select(id => new ContentTypeSort(id, sort++) { });
                            }
                        }
                    }

                    if (contentType.GetDirtyProperties().Count() > 0) contentType.ResetDirtyProperties();
                    contentTypeService.Save(contentType);
                }
                #endregion Create Permissions

            }
        }

        private void ProcessSitemap(ExcelPackage package)
        {
            var elementsSheets = package.Workbook.Worksheets.Where(x => x.Name.ToUpper().Contains("SITEMAP"));
            foreach (var ws in elementsSheets)
            {


                IContentService contentService = Services.ContentService;
                IContentTypeService contentTypeService = Services.ContentTypeService;

                int rowNums = ws.Dimension.Rows;
                int columnNums = ws.Dimension.Columns;

                Dictionary<int, int> parents = new Dictionary<int, int>();
                parents.Add(1, -1);

                long totalRecords = 0;
                var existingPages = contentService.GetPagedDescendants(-1, 0, 1000, out totalRecords).ToList();

                try
                {
                    for (int rowNum = 2; rowNum <= rowNums; rowNum++)
                    {
                        for (int columnNum = 1; columnNum <= columnNums; columnNum++)
                        {

                            var content = ExcelContentCellModel.GetFromCell(ws, rowNum, columnNum);
                            if (content != null)
                            {
                                IContent umbracoContent = existingPages?.FirstOrDefault(x => x.Name == content.Name_EL);
                                if (umbracoContent == null)
                                {
                                    int parentId = parents[content.Level];
                                    umbracoContent = contentService.Create(content.Name_EN, parentId, content.ContentTypeAlias);

                                    umbracoContent.CultureInfos = new ContentCultureInfosCollection() {
                                        new ContentCultureInfos("el") { Name = content.Name_EL },
                                        new ContentCultureInfos("en") { Name = content.Name_EN }
                                    };
                                    contentService.SaveAndPublish(umbracoContent);
                                }

                                if (parents.ContainsKey(content.Level + 1))
                                {
                                    parents[content.Level + 1] = umbracoContent.Id;
                                }
                                else
                                {
                                    parents.Add(content.Level + 1, umbracoContent.Id);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.Add(ex);
                }
            }
        }


        private bool TryGetContentTypeInfosFromRow(ExcelContentTypeRowModel row, ref IContentType contentType, string containerName = "", bool isElements = false)
        {

            try
            {
                IContentTypeService contentTypeService = Services.ContentTypeService;

                #region Process Content Type
                if (row.ContentTypeAlias.IsNotNullOrEmpty())//has content type
                {
                    if (!row.DeleteContentType)
                    {

                        //var parentElement = contentTypeService.Get(row.ElementParentAlias.IsNotNullOrEmpty() ? row.ElementParentAlias : containerName);
                        var parentElement = contentTypeService.Get(row.ContentTypeParentAlias);
                        var container = parentElement == null
                            ? Services.EntityService.GetDescendants(-1, UmbracoObjectTypes.DocumentTypeContainer).FirstOrDefault(x => x.Name == containerName)
                            : null;// contentTypeService.GetDescendants(-1, false).FirstOrDefault(x => x.Name == containerName);

                        //create containerFolder in case does not exist
                        if (parentElement == null && containerName.IsNotNullOrEmpty() && container == null)
                        {
                            //parentElement = new ContentType(-1) { Name = containerName, Alias = containerName, IsContainer = true };

                            //contentTypeService.Save(parentElement);
                            var entityContainer = contentTypeService.CreateContainer(-1, containerName);
                            container = Services.EntityService.GetDescendants(-1, UmbracoObjectTypes.DocumentTypeContainer).FirstOrDefault(x => x.Name == containerName);

                        }

                        var existingContentType = contentTypeService.Get(row.ContentTypeAlias);
                        if (existingContentType != null)
                        {
                            contentType = existingContentType;
                        }
                        else
                        {
                            if (parentElement != null)
                            {
                                contentType = new ContentType(parentElement, row.ContentTypeAlias);
                            }
                            else
                            {
                                contentType = new ContentType(-1)
                                {
                                    Alias = row.ContentTypeAlias
                                };
                            }
                        }
                        //case parent changed
                        if (parentElement != null && contentType.ParentId != parentElement.Id)
                        {
                            var existingParentElement = contentTypeService.Get(contentType.ParentId);
                            if (contentType.ContentTypeCompositionExists(existingParentElement?.Alias))
                            {
                                contentType.RemoveContentType(existingParentElement.Alias);
                            }
                            //contentType.ParentId = parentElement.Id;
                            contentType.SetParent(parentElement);
                            contentType.AddContentType(parentElement);

                        }
                        if (parentElement == null && container != null)
                        {
                            var existingParentElement = contentTypeService.Get(contentType.ParentId);
                            if (contentType.ContentTypeCompositionExists(existingParentElement?.Alias))
                            {
                                contentType.RemoveContentType(existingParentElement.Alias);
                            }
                            contentType.ParentId = container.Id;
                        }

                        contentType.IsElement = isElements;
                        contentType.Name = row.ContentTypeName;
                        contentType.Variations = row.ContentTypeMultilanguage ? ContentVariation.Culture : ContentVariation.Nothing;

                        //contentType.ParentId = parentElement.Id;
                    }
                    else
                    {
                        contentType = contentTypeService.Get(row.ContentTypeAlias);
                        if (contentType != null)
                        {
                            contentTypeService.Delete(contentType);
                            contentType = null;
                        }

                    }
                }
                #endregion Process Content Type

                #region Process Property

                if (row.PropertyAlias.IsNotNullOrEmpty() && contentType != null) //has property type
                {
                    PropertyType property = contentType.PropertyTypes.FirstOrDefault(x => x.Alias == row.PropertyAlias);
                    if (!row.DeleteProperty)
                    {
                        IDataType dataType = Services.DataTypeService.GetDataType(row.PropertyType);

                        bool isNewProperty = property == null;
                        if (isNewProperty)
                        {
                            property = new PropertyType(dataType)
                            {
                                Alias = row.PropertyAlias
                            };
                        }
                        property.Name = row.PropertyName;
                        property.Variations = row.PropertyMultilanguage ? ContentVariation.Culture : ContentVariation.Nothing;
                        if (row.PropertySort != 0)
                        {
                            property.SortOrder = row.PropertySort;
                        }

                        if (property.DataTypeId != dataType.Id)
                        {
                            property.DataTypeId = dataType.Id;
                            //property.DataTypeKey = dataType.Key;

                        }



                        var propertyGroupName = row.PropertyGroup.IsNotNullOrEmpty() ? row.PropertyGroup : "Content";
                        PropertyGroup propertyGroup = contentType.PropertyGroups.FirstOrDefault(x => x.Name == propertyGroupName);
                        if (propertyGroup == null)
                        {
                            propertyGroup = new PropertyGroup(true) { Name = propertyGroupName };
                            contentType.PropertyGroups.Add(propertyGroup);
                        }

                        // If group changed move to it
                        if (!isNewProperty
                            //&&contentType.PropertyGroups.Any(x => x.PropertyTypes.Contains(row.PropertyAlias)&&x.Name!= propertyGroupName))
                            && !propertyGroup.PropertyTypes.Contains(row.PropertyAlias))
                        {
                            var propertyCurrentGroup = contentType.PropertyGroups.FirstOrDefault(x => x.PropertyTypes.Contains(row.PropertyAlias));
                            propertyCurrentGroup.PropertyTypes.Remove(property);
                            propertyGroup.PropertyTypes.Add(property);

                            // delete group if no property
                            if (propertyCurrentGroup.PropertyTypes.Count == 0)
                            {
                                contentType.RemovePropertyGroup(propertyCurrentGroup.Name);
                            }
                        }

                        if (isNewProperty)
                        {
                            propertyGroup.PropertyTypes.Add(property);
                        }
                    }
                    else
                    {
                        if (property != null)
                            contentType.RemovePropertyType(property.Alias);
                    }
                }
                #endregion Process Property

                //contentTypeService.Save(contentType);
            }
            catch (Exception ex)
            {
                Exceptions.Add(ex);
            }

            return contentType != null || row.DeleteContentType;
        }

        private string GetTitleTemplate(string contentTypeFolderName, IContentType contentType)
        {
            string TitleTemplate = "";
            try
            {
                if (contentTypeFolderName.ToUpper().Contains("WIDGETS"))
                {

                    var dataType = Services.DataTypeService.GetDataType(contentTypeFolderName);
                    var dataTypeConfiguration = dataType?.Configuration as NestedContentConfiguration;
                    TitleTemplate = dataTypeConfiguration.ContentTypes.FirstOrDefault(x => x.Alias == contentType.Alias)?.Template;

                }
                else if (contentTypeFolderName.ToUpper().Contains("ELEMENTS"))
                {

                    var dataType = Services.DataTypeService.GetDataType(contentType + "s");
                    var dataTypeConfiguration = dataType?.Configuration as NestedContentConfiguration;
                    TitleTemplate = dataTypeConfiguration.ContentTypes.FirstOrDefault(x => x.Alias == contentType.Alias)?.Template;

                }
            }
            catch (Exception ex)
            {

            }

            return TitleTemplate;
        }


    }
}
