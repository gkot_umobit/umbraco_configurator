﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using Umbraco.Web.WebApi;

namespace UmbracoConfigurator.Controllers
{
    [PluginController("Umobit")]
    public class ResourcesController : UmbracoApiController
    {
        [HttpGet]//TODO: Make it play
        public FileStreamResult GetResource(string id)
        {
            id = id.Replace("$", "."); //Umbraco routing throws exceptions on using dot
            string resourceName = Assembly.GetExecutingAssembly().GetManifestResourceNames().ToList().FirstOrDefault(f => f.EndsWith(id));

            var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
            var contentType = GetContentType(id);

            var result = new FileStreamResult(resourceStream, contentType);
            return result;
        }

        //public ImageResult GetResource(string id)
        //{
        //    id = id.Replace("$", "."); //Umbraco routing throws exceptions on using dot
        //    string resourceName = Assembly.GetExecutingAssembly().GetManifestResourceNames().ToList().FirstOrDefault(f => f.EndsWith(id));

        //    var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);
        //    var contentType = GetContentType(id);

        //    var result = new ImageResult(resourceStream, contentType);
        //    return result;
        //}

        private string GetContentType(string fileId)
        {
            if (fileId.EndsWith(".js"))
            {
                return "text/javascript";
            }
            else if (fileId.EndsWith(".css"))
            {
                return "text/stylesheet";
            }
            else if (fileId.EndsWith(".jpg"))
            {
                return "image/jpeg";
            }
            else if (fileId.EndsWith(".png"))
            {
                return "image/png";
            }
            return "text";
        }
    }

    public class ImageResult : ActionResult
    {
        public ImageResult(Stream imageStream, string contentType)
        {
            if (imageStream == null)
                throw new ArgumentNullException("imageStream");
            if (contentType == null)
                throw new ArgumentNullException("contentType");

            this.ImageStream = imageStream;
            this.ContentType = contentType;
        }

        public Stream ImageStream { get; private set; }
        public string ContentType { get; private set; }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = this.ContentType;

            byte[] buffer = new byte[4096];
            while (true)
            {
                int read = this.ImageStream.Read(buffer, 0, buffer.Length);
                if (read == 0)
                    break;

                response.OutputStream.Write(buffer, 0, read);
            }

            response.End();
        }
    }
}
