﻿
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.Querying;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Umbraco.Web.PropertyEditors;
using UmbracoConfigurator.Helpers;
using UmbracoConfigurator.Models;

namespace Umobit.UmbracoConfigurator.Controllers
{
    public partial class UmobitUtilitiesController : UmbracoAuthorizedController //SurfaceController
    {


        public ActionResult DocumentTypeUsages(int nodeId)
        {
            List<UsagesModel> usages = new List<UsagesModel>();

            string documentTypeAlias = Services.ContentTypeService.Get(nodeId)?.Alias;

            if (documentTypeAlias.IsNotNullOrEmpty())
            {
                foreach (var root in Umbraco.ContentAtRoot())
                {

                    usages.AddRange(root.DescendantsOrSelf().Where(x => x.ContentType.Alias == documentTypeAlias).Select(x => new UsagesModel() { Id = x.Id, Name = x.Name }));
                    foreach (var node in root.DescendantsOrSelf())
                    {
                        var widgets = node.Properties.Where(x => x.PropertyType.EditorAlias == "Umbraco.NestedContent");
                        if (widgets.Count() > 0)
                        {
                            foreach (var widget in widgets)
                            {
                                var value = widget.Value();
                                if (value is IEnumerable<IPublishedElement>)
                                {
                                    usages.AddRange((value as IEnumerable<IPublishedElement>)
                                          .Where(x => x.ContentType.Alias == documentTypeAlias)
                                          .Select(x => new UsagesModel() { Id = node.Id, Name = node.Name + "(" + widget.Alias + ")" }));
                                }
                                else if (value is IPublishedElement)
                                {
                                    var elem = (value as IPublishedElement);
                                    if (elem.ContentType.Alias == documentTypeAlias)
                                    {
                                        usages.Add(new UsagesModel() { Id = node.Id, Name = node.Name + "(" + widget.Alias + ")" });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return View(new { Usages = usages, UsagesLinks = string.Join(", ", usages.Select(x => string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", x.Link, x.Name))) });
        }










    }




}
