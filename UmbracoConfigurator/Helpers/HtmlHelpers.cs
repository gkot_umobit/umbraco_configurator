﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP
{
    public static class HtmlHelpers
    {
        public static string EmbeddedResource(string name)
        {
            //TODO: Get this Working
            return "/umbraco/Umobit/Resources/GetResource/" + name.Replace(".", "$"); //Umbraco routing throws exceptions on using dot
        }
    }
}
