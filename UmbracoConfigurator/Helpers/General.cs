﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConfigurator.Helpers
{
    public static class General
    {
        public static bool HasValue(this ExcelRange range)
        {
            var value = range.Value;
            return value != null && !string.IsNullOrEmpty(value.ToString());
        }

        public static string StringValue(this ExcelRange range)
        {
            var value = range.Value;
            return range.Value?.ToString();
        }

        public static string GetStringValue(this ExcelWorksheet ws, int row, int column)
        {
            return ws.Cells[row, column].StringValue();
        }

        public static bool HasStringValue(this ExcelWorksheet ws, int row, int column)
        {
            return ws.Cells[row, column].HasValue();
        }

        public static bool IsNotNullOrEmpty(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        public static bool IsNotNullOrWhitespace(this string s)
        {
            return !string.IsNullOrWhiteSpace(s);
        }
    }
}
