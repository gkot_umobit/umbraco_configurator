﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;
using Umbraco.Core.Dashboards;
using Umbraco.Core.Composing;

namespace UmbracoConfigurator
{
    [Weight(200)]
    public class ConfiguratorDashboard : IDashboard
    {
        public string Alias => "Configurator";

        public string[] Sections => new[] {
            //"content",
            "settings"
        };

        public string View => "/umbraco/backoffice/UmbracoConfigurator/ConfiguratorDash";

        public IAccessRule[] AccessRules => Array.Empty<IAccessRule>();

        public ConfiguratorDashboard()
        {
            
        }
    }
}
