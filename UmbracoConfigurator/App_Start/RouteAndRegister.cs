﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Configuration;
using Umbraco.Web.Trees;
using Umobit.UmbracoConfigurator.Controllers;

namespace UmbracoConfigurator.App_Start
{
    public class RegisterCustomBackofficeMvcRouteComponent : Umbraco.Core.Composing.IComponent
    {
        private readonly IGlobalSettings _globalSettings;
        public RegisterCustomBackofficeMvcRouteComponent(IGlobalSettings globalSettings)
        {
            _globalSettings = globalSettings;
        }


        public void Initialize()
        {
            ContentTreeController.MenuRendering += ContentTreeController_MenuRendering;

            RouteTable.Routes.MapRoute("UmbracoConfigurator", _globalSettings.GetUmbracoMvcArea() + "/backoffice/UmbracoConfigurator/{action}", new
            {
                controller = "UmbracoConfigurator",
                //action = "meow",
                id = UrlParameter.Optional
            });

            RouteTable.Routes.MapRoute("UmobitUtilities", _globalSettings.GetUmbracoMvcArea() + "/backoffice/UmobitUtilities/{action}", new
            {
                controller = "UmobitUtilities",
                //action = "meow",
                id = UrlParameter.Optional
            });
        }

        public void Terminate()
        {
            //throw new NotImplementedException();
        }

        private void ContentTreeController_MenuRendering(TreeControllerBase sender, MenuRenderingEventArgs e)
        {
            switch (sender.TreeAlias)
            {
                case "content":
                    var i = new Umbraco.Web.Models.Trees.MenuItem("ExportNodeDescendantsToExcel", "Export Node Descendants To Excel");
                    //optional, if you dont want to follow conventions, but do want to use a angular view
                    i.AdditionalData.Add("actionView", "/umbraco/backoffice/UmobitUtilities/ExportNodeDescendantsToExcelView?nodeId=" + e.NodeId);
                    i.Icon = "user-females-alt";
                    e.Menu.Items.Insert(e.Menu.Items.Count, i);
                    break;

                case "documentTypes":
                    var di = new Umbraco.Web.Models.Trees.MenuItem("DocumentTypeUsages", "Find Usages");
                    //optional, if you dont want to follow conventions, but do want to use a angular view
                    di.AdditionalData.Add("actionView", "/umbraco/backoffice/UmobitUtilities/DocumentTypeUsages?nodeId=" + e.NodeId);
                    di.Icon = "layers";
                    e.Menu.Items.Insert(e.Menu.Items.Count, di);
                    break;
            }
        }
    }

    public class RegisterCustomBackofficeMvcRouteComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            composition.Register<UmbracoConfiguratorController>(Lifetime.Request);
            composition.Register<UmobitUtilitiesController>(Lifetime.Request);

            composition.Components().Append<RegisterCustomBackofficeMvcRouteComponent>();

        }
    }

}
