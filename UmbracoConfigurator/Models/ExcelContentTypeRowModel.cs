﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using UmbracoConfigurator.Helpers;

namespace UmbracoConfigurator.Models
{
    public class ExcelContentTypeRowModel
    {
        public ExcelContentTypeRowModel(ExcelWorksheet ws, int row)
        {
            ContentTypeName = ws.GetStringValue(row, 1) ?? "";
            ContentTypeAlias = ws.GetStringValue(row, 2) ?? "";
            ContentTypeParentAlias = ws.GetStringValue(row, 3) ?? "";
            ContentTypeMultilanguage = ws.GetStringValue(row, 4)?.ToUpper() == "TRUE";
            ContentTypeTitleTemplate = ws.GetStringValue(row, 5) ?? "";
            ContentTypePermissions = ws.GetStringValue(row, 6) ?? "";
            DeleteContentType = ws.GetStringValue(row, 7)?.ToUpper() == "TRUE";
            PropertyName = ws.GetStringValue(row, 8) ?? "";
            PropertyAlias = ws.GetStringValue(row, 9) ?? "";
            PropertyType = ws.GetStringValue(row, 10) ?? "";
            PropertyMultilanguage = ws.GetStringValue(row, 11)?.ToUpper() == "TRUE";
            PropertyGroup = ws.GetStringValue(row, 12) ?? "";
            int sort = 0;
            int.TryParse(ws.GetValue(row, 13)?.ToString(), out sort);
            PropertySort = sort;//ws.GetValue(row, 13)?.GetType() == typeof(int) ? ws.GetValue<int>(row, 13) : 0;//fix
            DeleteProperty = ws.GetStringValue(row, 14)?.ToUpper() == "TRUE";

        }

        public ExcelContentTypeRowModel(IContentType ct, string parentAlias, string titleTemplate)
        {
            List<string> permissions = new List<string>();
            if (ct.AllowedAsRoot) permissions.Add("root");
            permissions = permissions.Union(ct.AllowedContentTypes.Select(x => x.Alias).ToList()).ToList();

            ContentTypeName = ct.Name;
            ContentTypeAlias = ct.Alias;
            ContentTypeParentAlias = parentAlias;
            ContentTypeMultilanguage = ct.Variations == ContentVariation.CultureAndSegment ? true : false;
            ContentTypeTitleTemplate = titleTemplate;
            ContentTypePermissions = permissions.Count > 0 ? string.Join(",", permissions) : "";

            DeleteContentType = false;
            DeleteProperty = false;


        }

        public ExcelContentTypeRowModel(PropertyType pt, string propertyGroup, string propertyDataType)
        {
            PropertyName = pt.Name;
            PropertyAlias = pt.Alias;
            PropertyType = propertyDataType;
            PropertyMultilanguage = pt.Variations == ContentVariation.Culture ? true : false;
            PropertyGroup = propertyGroup;
            PropertySort = pt.SortOrder;
        }

        public ExcelRange ToExcelRange(ExcelWorksheet ws)
        {
            ws.Cells.LoadFromCollection(new List<ExcelContentTypeRowModel>(), true);
            return null;
        }

        public string ContentTypeName { get; private set; }
        public string ContentTypeAlias { get; private set; }
        public string ContentTypeParentAlias { get; private set; }
        public bool ContentTypeMultilanguage { get; private set; }
        public string ContentTypeTitleTemplate { get; private set; }
        public string ContentTypePermissions { get; private set; }
        public bool DeleteContentType { get; private set; }
        public string PropertyName { get; private set; }
        public string PropertyAlias { get; private set; }
        public string PropertyType { get; private set; }
        public bool PropertyMultilanguage { get; private set; }
        public string PropertyGroup { get; private set; }
        public int PropertySort { get; private set; }
        public bool DeleteProperty { get; private set; }
        //Content Type Title Template
    }
}
