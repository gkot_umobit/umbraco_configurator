﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UmbracoConfigurator.Models
{
    public class UsagesModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get { return "/umbraco#/content/content/edit/" + Id; } }
    }
}
