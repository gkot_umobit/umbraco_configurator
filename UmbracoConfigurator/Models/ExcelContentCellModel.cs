﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using UmbracoConfigurator.Helpers;

namespace UmbracoConfigurator.Models
{
    public class ExcelContentCellModel
    {
        public ExcelContentCellModel()
        {



        }

        public static ExcelContentCellModel GetFromCell(ExcelWorksheet ws, int row, int column)
        {

            string cellValue = ws.GetStringValue(row, column) ?? "";
            string[] cellArray = cellValue.Split('|');
            if (cellValue.IsNotNullOrEmpty() && cellArray.Count() == 3)
            {

                return new ExcelContentCellModel()
                {
                    Name_EL = cellArray[0],
                    Name_EN = cellArray[1],
                    ContentTypeAlias = cellArray[2],
                    Level = column,
                };
            }
            else
            {
                return null;
            }

        }





        public string Name_EL { get; private set; }
        public string Name_EN { get; private set; }
        public string ContentTypeAlias { get; private set; }
        public int Level { get; private set; }

    }
}
